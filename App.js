//Loading File Sys
const fs = require("fs");
//Loading package.json info
var pkginfo = require("pkginfo")(module, "name", "version", "author");
//Loading readline
const readline = require("readline");

logCredentials();

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Enter absolute text file path: ", function (path) {
  fs.readFile(path, "utf8", function (err, data) {
    if (err === null) {
      rl.question("Enter output destination folder: ", function (dest) {
        if (err === null) {
          console.log();
          console.log("Creating output file in folder " + dest);
          console.log();

          var translationKeys = data.match(
            /(?<=Translate\s)[a-zA-Z0-9+]+(?=\})/g
          );

          var translations = [];

          translationKeys.forEach((key) => {
            if(!translations.some(e => e.TranslationKey === key)){
              translations.push({
                TranslationKey: key,
                TranslationCZ: "",
                TranslationEng: "",
              });
            }
          });

          console.log(dest);

          if (dest.trim() !== "") {
            fs.writeFile(
              dest + "\translations-json.txt",
              JSON.stringify(translations),
              function (err) {
                if (err === null) {
                  console.log("File was created in selected destination");
                } else {
                  console.log(err.message);
                }
              }
            );
          } else {
            fs.writeFile(
              "translations-json.txt",
              JSON.stringify(translations),
              function (err) {
                if (err === null) {
                  console.log("File was created in application folder");
                } else {
                  console.log(err.message);
                }
              }
            );
          }

          rl.close();
        } else {
          console.log(err.message);
          rl.close();
        }
      });
    } else {
      console.log(err.message);
      rl.close();
    }
  });
});

rl.on("close", function () {});

function logCredentials() {
  console.log();
  console.log("Initializing application " + module.exports.name);
  console.log("Version: " + module.exports.version);
  console.log("Made by: " + module.exports.author);
  console.log();
}
